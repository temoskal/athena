#include "TrigInDetTrackFitter/TrigInDetTrackFitter.h"
#include "TrigInDetTrackFitter/TrigInDetBremDetectionTool.h"
#include "TrigInDetTrackFitter/TrigDkfTrackMakerTool.h"
#include "TrigInDetTrackFitter/TrigL2ResidualCalculator.h"
#include "TrigInDetTrackFitter/TrigInDetOfflineTrackFitter.h"
#include "TrigInDetTrackFitter/TrigInDetCombinedTrackFitter.h"
#include "TrigInDetTrackFitter/TrigL2HighPtTrackFitter.h"
#include "TrigInDetTrackFitter/TrigL2LowPtTrackFitter.h"
#include "TrigInDetTrackFitter/TrigL2FastExtrapolationTool.h"


DECLARE_COMPONENT( TrigInDetTrackFitter )
DECLARE_COMPONENT( TrigInDetBremDetectionTool )
DECLARE_COMPONENT( TrigDkfTrackMakerTool )
DECLARE_COMPONENT( TrigL2ResidualCalculator )
DECLARE_COMPONENT( TrigInDetOfflineTrackFitter )
DECLARE_COMPONENT( TrigInDetCombinedTrackFitter )
DECLARE_COMPONENT( TrigL2HighPtTrackFitter )
DECLARE_COMPONENT( TrigL2LowPtTrackFitter )
DECLARE_COMPONENT( TrigL2FastExtrapolationTool )

